from bs4 import BeautifulSoup
from dotenv import load_dotenv
from imap_tools import MailBox, AND
import json
import os
import requests

load_dotenv()
gitea_users = os.getenv('GITEA_USERS').split()
gitea_url = f'''{os.getenv('GITEA_URL')}/api/v1/repos/{os.getenv('GITEA_ORG')}/{os.getenv('GITEA_PROJECT')}/issues?token={os.getenv('GITEA_TOKEN')}'''
headers = headers = {'Content-type': 'application/json', 'Accept': 'application/json'}
#print(gitea_url)
with MailBox(os.getenv('EMAIL_SERVER')).login(os.getenv('EMAIL_USER'),os.getenv('EMAIL_PASSWORD')) as mailbox:
    processed_messages = [] # used to prevent multiple emails from returning invalid messages set
    for msg in mailbox.fetch():
        #print(msg.date,msg.subject,msg.html)

        if msg.html == '':
            request_body = {"assignees": gitea_users,"body":f'''{msg.date} {msg.from_} {msg.text}''',"title": msg.subject}
        else:
            soup = BeautifulSoup(msg.html,features="lxml")
            #print(soup.get_text()) # check beautiful soup output
            request_body = {"assignees": gitea_users,"body":f'''{msg.date} {msg.from_} {soup.get_text()}''',"title": msg.subject}
        #print(request_body)
        r = requests.post(gitea_url,json=request_body,headers=headers)
        if r.status_code == 201:
            print(f'''{msg.subject} has been created in {os.getenv('GITEA_PROJECT')} now moving issues to processed''')
            processed_messages.append(msg.uid)
        else:
            print(r.status_code,r.text)
    print('message fetching completed')
    if processed_messages:
        print('now cleaning up mailbox')
    for message in processed_messages:
        try:
            print(f'''moving {msg.subject} to processed''')
            mailbox.move(message,'INBOX/Processed')
        except:
            print('creating processed folder')
            mailbox.folder.create('INBOX/Processed')
            print(f'''moving {msg.subject} to processed''')
            mailbox.move(message,'INBOX/Processed')
