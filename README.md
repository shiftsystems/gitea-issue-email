# Gitea Issue Email

designed to read email from an imap inbox and create an issue inside a gitea project

## Getting started

This repo will need to be cloned in in /opt/ for the timer to work correctly Eventually I will make a container or ansible role to automate this because it is $CURRENT_YEAR

1. Install os dependancies by running ```sudo dnf install python3-pip python3-requests python3-beautifulsoup```
2. cd to /opt and run git clone https://gitlab.com/shiftsystems/gitea-issue-email.git
3. cd into /opt/gitea-email-issue and install requirements by running sudo pip3 install -r requirements.txt 
4. edit vaules in the .env file to match your envrionment
5. copy the .timer and .service file to /etc/systemd/system
6. start enable the timer by running sudo systemctl enable --now gitea-email-issue.timer
7. test by sending an email to address to placed in the .env file
8. profit
